myAVR-MK2 - C example - PWM, LED
================================

 A simple C example, dimming a LED by PWM, using an ATmega8L controller.


[Datasheet]: https://www.microchip.com/wwwproducts/en/ATmega8 "ATmega8(L) Datasheet"
[AVR Guide PWM]: https://sites.google.com/site/qeewiki/books/avr-guide/pwm-atmega8 "AVR Guide - PWM"


References
----------

 - ATmega8(L) - Complete [Datasheet], Rev. **2486AA-AVR-02/2013**
 - [AVR Guide PWM]


Pin Configuration
-----------------

### PDIP

	       ---xx---
	PC6 --| 1    28|-- PC5
	PD0 --| 2    27|-- PC4
	PD1 --| 3    26|-- PC3
	PD2 --| 4    25|-- PC2
	PD3 --| 5    24|-- PC1
	PD4 --| 6    23|-- PC0
	VCC --| 7    22|-- GND
	GND --| 8    21|-- AREF
	PB6 --| 9    20|-- AVCC
	PB7 --|10    19|-- PB5
	PD5 --|11    18|-- PB4
	PD6 --|12    17|-- PB3 ----[ R ]----LED>|----GND
	PD7 --|13    16|-- PB2
	PB0 --|14    15|-- PB1
	       --------


Registers
---------

### Timer/Counter Control Register **TCCR2**

 *[Datasheet], Page 114*


 |      7 |      6 |      5 |      4 |      3 |      2 |      1 |      0 |
 | -----: | -----: | -----: | -----: | -----: | -----: | -----: | -----: |
 |   FOC2 |  WGM20 |  COM21 |  COM20 |  WGM21 |   CS22 |   CS21 |   CS20 |

 - *Bit 7*    - **FOC2**:    Force Output Compare
 - *Bit 6:3*  - **WGM21:0**: Waveform Generation Mode
 - *Bit 5:4*  - **COM21:0**: Compare Match Output Mode
 - *Bit 2..0* - **CS22..0**: Clock Select


#### Clock Select Bit Description

 *[Datasheet], Page 116*


 | CS20 | CS21 | CS20 | Description           |
 | ---: | ---: | ---: | :-------------------- |
 |    0 |    0 |    0 | Timer/Counter stopped |
 |    0 |    0 |    1 | No Prescaler          |
 |    0 |    1 |    0 | Clock / 8             |
 |    0 |    1 |    1 | Clock / 32            |
 |    1 |    0 |    0 | Clock / 64            |
 |    1 |    0 |    1 | Clock / 128           |
 |    1 |    1 |    0 | Clock / 256           |
 |    1 |    1 |    1 | Clock / 1024          |


#### Waveform Generation Mode Bit Description

 *[Datasheet], Page 115*


 | Mode | WGM21 | WGM20 | Description         |
 | :--- | ----: | ----: | :------------------ |
 |    0 |     0 |     0 | Normal              |
 |    1 |     0 |     1 | PWM Phase Corrected |
 |    2 |     1 |     0 | CTC                 |
 |    3 |     1 |     1 | Fast PWM            |


#### Compare Output Mode, Fast PWM Mode

 *[Datasheet], Page 115*


 | COM21 | COM20 | Description                                       |
 | ----: | ----: | :------------------------------------------------ |
 |     0 |     0 | OC2 disabled                                      |
 |     0 |     1 | Reserved                                          |
 |     1 |     0 | None-inverted mode (HIGH at bottom, LOW on match) |
 |     1 |     1 | Inverted mode      (LOW at bottom, HIGH on match) |


### Timer/Counter Interrupt Flag Register **TIMSK**

 *[Datasheet], Page 117*


 |      7 |      6 |      5 |      4 |      3 |      2 |      1 |      0 |
 | -----: | -----: | -----: | -----: | -----: | -----: | -----: | -----: |
 |  OCIE2 |  TOIE2 | TICIE1 | OCIE1A | OCIE1B |  TOIE1 |      - |  TOIE0 |


 - *Bit 7* - **OCIE2**: Timer/Counter2 Output Compare Match Interrupt Enable
 - *Bit 6* - **TOIE2**: Timer/Counter2 Output Interrupt Enable


### Timer/Counter Interrupt Flag Register **TIFR**

 *[Datasheet], Page 119*


 |      7 |      6 |      5 |      4 |      3 |      2 |      1 |      0 |
 | -----: | -----: | -----: | -----: | -----: | -----: | -----: | -----: |
 |   OCF2 |   TOV2 |   ICF1 |  OCF1A |  OCF1B |   TOV1 |      - |   TOV0 |


 - *Bit 7* - **OCF2**: Output Compare Flag 2
 - *Bit 6* - **TOV2**: Timer/Counter2 Overflow Flag


### Timer/Counter Register **TCNT2**

 *[Datasheet], Page 116*


 stores the counter value


### Output Compare Register **OCR2**

 *[Datasheet], Page 116*


 stores the compare value

