/* myAVR - C example - PWM, LED
 * ============================
 *
 * A simple C example, dimming a LED by PWM, using an ATmega8L controller.
 */


/** Main program.
 *
 * @file   main.c
 * @date   2019
 * @author Martin Singer
 */

#ifndef F_CPU
  #define F_CPU 3686400ul
#endif


#include <avr/io.h>
#include <util/delay.h> // requires F_CPU


/** Main function.
 */
int main(void)
{
	DDRB |= (1<<DDB3);                // set port PB3 as output
	OCR2 = 0x00;                      // set PWM for 00% duty cycle
	TCCR2 |= (1<<COM21);              // set none-inverting mode
	TCCR2 |= (1<<WGM21) | (1<<WGM20); // set fast PWM mode
	TCCR2 |= (1<<CS21);               // set prescaler to 8 and start PWM

	do {
		for (int i = 0x00; i < 0xFF; ++i) {
			OCR2 = i;
			_delay_ms(5);
		}
		for (int i = 0xFF; i > 0x00; --i) {
			OCR2 = i;
			_delay_ms(5);
		}
	} while (1);

	return 0;
}

